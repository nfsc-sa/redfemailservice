﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REDFEmailService.Data
{
    class ExcelFile
    {
        public static string ExportDataSetToExcel(DataTable ds)
        {
            string[] selectedColumns = new[] { "Account", "Natioanl_ID", "Instalment_No", "Response" };

            DataTable dt = new DataView(ds).ToTable(false, selectedColumns) ;
            dt.TableName = "REDFFailedResponse";
           
            string AppLocation = "";
            AppLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            AppLocation = AppLocation.Replace("file:\\", "");
            string file = AppLocation + "\\ExcelFiles\\REDFFailedResponse.xlsx";
            if(System.IO.File.Exists(file)){
                System.IO.File.Delete(file);
            }
            using (XLWorkbook workbook = new XLWorkbook())
            {
               workbook.Worksheets.Add(dt);
                
                workbook.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                workbook.Style.Font.Bold = true;
                workbook.SaveAs(file);
            }
            return file;
        }
    }
}
