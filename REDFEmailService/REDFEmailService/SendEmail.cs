﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using REDFEmailService.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace REDFEmailService
{
    public class SendEmail
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Email(string ToEmail)
        {
            try
            {
                DataTable list = DBOperations.GetREDFTransactionInfo();
                log.Info("List of account count is " + list.Rows.Count);
                if (list.Rows.Count > 0)
                {

                    string Filepath = ExcelFile.ExportDataSetToExcel(list);
                    System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();

                    smtpClient.Host = ConfigurationManager.AppSettings["Host"].ToString();
                    smtpClient.Port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["PasswordEmail"]);
                    smtpClient.EnableSsl = true;
                    smtpClient.Timeout = 200000;
                    bool result = false;
                    using (MailMessage MailMsg = new MailMessage())
                    {


                        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");

                        string strBody = ConfigurationManager.AppSettings["Message"];

                        MailMsg.BodyEncoding = System.Text.Encoding.Default;
                        MailMsg.To.Add(ToEmail);
                        MailMsg.Priority = System.Net.Mail.MailPriority.High;
                        MailMsg.Subject = ConfigurationManager.AppSettings["Subject"];
                        MailMsg.Body = strBody;
                        MailMsg.IsBodyHtml = true;

                        MailMsg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"]);
                        FileInfo fileInfo = new FileInfo(Filepath);

                        byte[] fileBytes = new UTF8Encoding(true).GetBytes(Filepath);
                        Attachment _attachment = new Attachment(Filepath);
                        MailMsg.Attachments.Add(_attachment);

                        System.Net.Mail.AlternateView HTMLView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, HTMLType);

                        try
                        {
                            //MailMsg.UseDefaultCredentials = true;
                            smtpClient.Send(MailMsg);
                            result = true;
                            _attachment.Dispose();
                            log.Info("Mail sent successfully!");


                        }
                        catch (Exception ex)
                        {
                            log.Info(string.Format("Exception caught in send Mail: {0}", ex.ToString()));
                        }
                    }
                    if (result == true)
                    {
                        DBOperations.UpdateEmailTracking(list);

                    }


                }





            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
                throw;
            }
        }


    
      
    }
}
