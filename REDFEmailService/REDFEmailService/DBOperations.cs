﻿using REDFEmailService.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REDFEmailService
{
    class DBOperations
    {
        public static DataTable GetREDFTransactionInfo()
        {
            // Database db = DatabaseFactory.CreateDatabase();
            //DbCommand dbCommand = db.GetSqlStringCommand("select * from [PaymentConfirmationSMSTracking]");//db.GetStoredProcCommand("CasePartiesGetDetailsByCaseInfoID");
            return SqlHelper.Select("LMSConnString", "GetRedfAccListforemail", CommandType.StoredProcedure);
            // db.AddInParameter(dbCommand, "CaseInfoID", DbType.Int32, caseInfoID); ;

            // return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static void UpdateEmailTracking(DataTable dataTable)
        {
            string[] selectedColumns = new[] { "Id" };

            DataTable data = new DataView(dataTable).ToTable(false, selectedColumns) as DataTable;
            var parameters = new[] {
                            new SqlParameter("@REDFEmailSend",data),




            };
            var dbResult = SqlHelper.Scalar("LMSConnString", "UpdateEmailTracking", CommandType.StoredProcedure, parameters);


        }

    }
}
